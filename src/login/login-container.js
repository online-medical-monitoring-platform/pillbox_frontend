import React from 'react';
import validate from "../patient/components/validators/patient-validators";
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import {FormGroup, Input, Label} from 'reactstrap';
import {Redirect} from "react-router-dom";
import * as API_USERS from "../user/user-api";


class LoginContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            redirectTo:'',
            selectedLogin: false,
            errorStatus: 0,
            error: null,

            user:null,
            userId: '',
            userType:'',
            formIsValid: false,

            loginValid:false,

            doctorList: [],
            caregivers: [],
            patients: [],

            formControls: {
                username: {
                    value: '',
                    placeholder: 'username',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                password: {
                    type: "password",
                    value: '',
                    placeholder: 'password',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
            }
        };

        // this.reloadLogin = this.reloadLogin.bind(this);
        //  this.login = this.login.bind(this);
        //  this.checkUser= this.checkUser.bind(this);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.reloadLogin=this.reloadLogin.bind(this);
    }

    reloadLogin(){
        this.setState({selectedLogin: !this.state.selectedLogin});
    }



    checkUser (username, password)
    {console.log("checkuser "+username+" "+password);
        return API_USERS.getUserByDetails(username, password,(result, status, err) => {
            let userFound;
            if (result !== null && status === 200) {
                userFound=result;
            } else {
                if(result == null) {
                    console.log(username + password + "Nu e pacient");
                    localStorage.setItem('role', 'notFound');
                }
            }
            this.setState({
                user:userFound
            })
            if(!(result == null))
                if(!(userFound.role==="notFound")) {
                    localStorage.setItem('id', userFound.id);
                    localStorage.setItem('role', userFound.role);
                    if (userFound.role === 'patient')
                        this.setState({
                            redirectTo: '/patient'
                        })
                    this.reloadLogin();
                }


        });

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    handleSubmit() {
        let user = {
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value
        };
        this.checkUser(user.username, user.password);


        // this.reloadLogin();

        /*   if(!(user.username==="doctor"))
               localStorage.setItem('role', '');
               else localStorage.setItem('role', 'doctor');       /*   if(user.username==="patient") {
               localStorage.setItem('role', 'patient');
               localStorage.setItem('username', user.username);
               localStorage.setItem('password', user.password);
           }*/

    }

    render() {

        if (localStorage.getItem('role')==="patient")
            return <Redirect to='/patient'/>
        else
            return (
                <div className="container">
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <Row>
                        <Col sm={{size: '9', offset: 4}}>
                            <FormGroup id='username' text-align={'center'} >
                                <Label for='usernameField'> Username: </Label>
                                <Input name='username' id='usernameField'
                                       placeholder={this.state.formControls.username.placeholder}
                                       onChange={this.handleChange}
                                       width={'10'}
                                       defaultValue={this.state.formControls.username.value}
                                       touched={this.state.formControls.username.touched ? 1 : 0}
                                       valid={this.state.formControls.username.valid}
                                       required
                                />
                                {this.state.formControls.username.touched && !this.state.formControls.username.valid &&
                                <div className={"error-message row"}> * Username must have at least 3 characters </div>}
                            </FormGroup>
                        </Col>
                    </Row>

                    <Row>
                        <Col sm={{size: '9', offset: 4}}>
                            <FormGroup id='password'>
                                <Label for='passwordField'> Password: </Label>
                                <Input name='password' id='passwordField'
                                       placeholder={this.state.formControls.password.placeholder}
                                       type="password"
                                       onChange={this.handleChange}
                                       size={'10'}
                                       defaultValue={this.state.formControls.password.value}
                                       touched={this.state.formControls.password.touched ? 1 : 0}
                                       valid={this.state.formControls.password.valid}
                                       required
                                />
                                {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
                                <div className={"error-message"}> * Password must have at least 3 characters </div>}
                            </FormGroup>

                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '4',offset: '5'}} style={{marginLeft:'510px'}}>
                            <Button type={"submit"} disabled={!this.state.formIsValid}
                                    onClick={this.handleSubmit}> Login </Button>
                        </Col>
                    </Row>
                    <br/>
                    {
                        localStorage.getItem('role')==='notFound' &&
                        <Label style={{color:'red',fontSize:'25',marginLeft:'390px'}}>
                            <b>Invalid username or password for patient!</b></Label>
                    }
                    {
                        this.state.errorStatus > 0 &&
                        <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                    }
                </div>
            );

    }

}

export default LoginContainer;
