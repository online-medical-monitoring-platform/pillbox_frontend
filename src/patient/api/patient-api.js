import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/pillbox/patient',
    medicationPlan: '/medicationPlan'
};

function getPatientById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.patient +"/"+ params, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function getMedicationPlans(params, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + "/"+params+ endpoint.medicationPlan, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getPatientById,
    getMedicationPlans
};
