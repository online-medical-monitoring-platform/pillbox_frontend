import React from 'react';
import {Label
} from 'reactstrap';

import PatientMedicationPlans from "./components/patient-medicationPlans";
import * as API_PLANS from "../medicationPlan/medicationPlan-api";



class PatientDetails extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            patient: this.props.patient,
            plans:this.props.medicationPlans,
            hour:this.props.hour,
            minute:this.props.minute,
            prescriptions:[],
            show: false,

            id: 0,
            selected: false,
            collapseForm: false,
            collapseFormUpdate:false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            caregivers:[],
            filteredPlans:[]
        }
        this.reload = this.reload.bind(this);

    }

    componentDidMount() {
        //this.findPlans();
    }

    reload() {
        if(this.state.isLoadedPlans) {
            this.setState({
                show: false
            });
        }
    }

    fetchMedicalPrescriptions(id){

        return API_PLANS.getMedicationPrescriptions(id,(result, status, err) => {
            if (result !== null && status === 200) {
                for(let i=0;i<result.length;i++) {
                    console.log("Din ficecare prescriptie")
                    let tempIntake = result[i].periodStop.split("-");
                    if(parseInt(tempIntake[0])<=parseInt(this.state.hour) && parseInt(tempIntake[1])>parseInt(this.state.hour)) {
                        this.state.prescriptions.push(result[i])
                        console.log("Din ficecare prescriptie")
                    }
                }
                this.setState({
                    prescriptions:result,
                    show: true
                })
                console.log("Din fetch"+this.state.prescriptions[0]);
             }else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }



    render() {

        //  console.log(this.props.plans.indexOf(0));
        console.log("din details");
        console.log(this.state.plans);

        //console.log("in render " +this.state.specificPlans);
        const columns = [
            {
                Header: "Name",
                accessor: "name",
                style:{
                    textAlign: "center"
                },
                width: 250
            },
            {
                Header: "Birthdate",
                accessor: "birthdate",
                style:{
                    textAlign: "center"
                },
                width: 150
            },
            {
                Header: "Gender",
                accessor: "gender",
                style:{
                    textAlign: "center"
                },
                width: 100
            },
            {
                Header: "Address",
                accessor: "address",style:{
                    textAlign: "center"
                },
                width: 300
            }]


        return (
            <div >
                <Label sm={{ offset: 1}}><b>Name: </b>{this.state.patient.name}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Birthdate: </b>{this.state.patient.birthdate}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Gender: </b>{this.state.patient.gender}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Address: </b>{this.state.patient.address}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Medical record : </b>{this.state.patient.medicalRecord}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Username: </b>{this.state.patient.username} <b>Password: </b>{this.state.patient.password}</Label>
                <br/>

                {  this.state.plans.map(
                    plan=> (
                         <div key={plan.id} style={{backgroundColor:'#fff3d4', padding:'30px'}}>
                            <Label sm={{offset: 1} } ><b>Medical plan name : </b>{plan.name}</Label>
                            <br/>
                            <Label sm={{offset: 1} } >Period: {" " +plan.periodStart +" <---> "+plan.periodStop}</Label>
                            <br/>
                            <PatientMedicationPlans medicationPlan={plan} hour={this.state.hour}/>
                        </div>
                    ))}
            </div>
        )

    }
}

export default PatientDetails;
