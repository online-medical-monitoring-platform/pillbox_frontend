import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader,
} from 'reactstrap';
import * as API_PATIENTS from "../patient/api/patient-api";
import PatientDetails from "./patient-details";
import {Redirect} from "react-router-dom";



class PatientPage extends React.Component {


    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.logoutHandle=this.logoutHandle.bind(this);

        this.state = {
            id: '',
            selected: false,
            plans: [],
            isLoaded: false,
            isLoadedPlans: false,
            patient:null,
            redirect:false,
            errorStatus: 0,
            error: null,
            time: new Date().toLocaleString(),
            day: new Date().getDate().toLocaleString(),
            month: (new Date().getMonth()+1).toLocaleString(),
            hour: new Date().getHours().toLocaleString(),
            minute: new Date().getMinutes().toLocaleString()

        }

    }

    logoutHandle(){
        localStorage.clear();
        this.setState({
            redirect:true
        })
    }

    componentDidMount() {
        this.fetchMedicationPlans();
        this.fetchPatient();
        this.intervalID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.intervalID);
    }
    tick() {
        let date= new Date()
        this.setState({
            time: date.toLocaleString().toLocaleString(),
            day: date.getDate().toLocaleString(),
            month: (date.getMonth()+1).toLocaleString(),
            hour: date.getHours().toLocaleString(),
            minute: date.getMinutes().toLocaleString()
        });
       /* console.log("Day "+this.state.day)
        console.log("Month "+this.state.month)
        console.log("Hour "+this.state.hour)
        console.log("Minute "+this.state.minute)
        if(this.state.day==='9')
            console.log("COMPARA BINE")
        else
            console.log("NU COMPARA BINE")
        */

    }

    fetchPatient() {
        return API_PATIENTS.getPatientById(Number(localStorage.getItem('id')),(result, status, err) => {
            //console.log("Am gasit pacientu "+result.name)
            if (result !== null && status === 200) {
                this.setState({
                    patient: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchMedicationPlans() {
        return API_PATIENTS.getMedicationPlans(Number(localStorage.getItem('id')),(result, status, err) => {
            if (result !== null && status === 200) {
                for(let i=0;i<result.length;i++){
                    let  tempDataStop = result[i].periodStop.split("-");
                    let  tempDataStart = result[i].periodStart.split("-");

                    let startDate= new Date(tempDataStart[2],tempDataStart[1]-1,tempDataStart[0])
                    let stopDate= new Date(tempDataStop[2],tempDataStop[1]-1,tempDataStop[0])
                    let today=new Date()
                    if(startDate.getTime()<= today.getTime() && today.getTime()<=stopDate.getTime())
                    {
                        this.state.plans.push(result[i])}
                }

                console.log("Am gasit planuri medicale din result"+ result.length+" cate raman"+this.state.plans.length )
                this.setState({
                    isLoadedPlans: true
                })
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
        if(this.state.isLoadedPlans) {
            this.setState({
                isLoaded: false,
                isLoadedPlans: false
            });
            this.toggleForm();
            this.fetchMedicationPlans();
            this.fetchPatient();
        }
    }



    render() {
        /*   console.log("in render in page");
           console.log(this.state.patient);
           console.log("in render in page plan[0]");
           console.log(this.state.plans);
   */
        if(this.state.redirect)
        {
            return <Redirect to={'/login'}/>;
        }
        console.log("in render in patientPage");
        return (
            <div>
                <CardHeader>
                    <strong style={{fontSize:'30px'}}> Patient  </strong>
                    <button style={{marginLeft:'20px'}} onClick={this.logoutHandle} >Log out</button>
                    <p style={{fontSize:'20px'}}>
                        {this.state.time}
                    </p>
                </CardHeader>
                <Card style={{backgroundColor:'#ede7d8'}}>
                    {this.state.isLoadedPlans && this.state.isLoaded &&  <PatientDetails  patient={this.state.patient}
                                                                                          medicationPlans={this.state.plans}
                                                                                          hour={this.state.hour}
                                                                                          minute={this.state.minute}/>}
                    {this.state.errorStatus > 0 && <APIResponseErrorMessage
                        errorStatus={this.state.errorStatus}
                        error={this.state.error}
                    />   }

                </Card>
            </div>
        )

    }
}
export default PatientPage;
