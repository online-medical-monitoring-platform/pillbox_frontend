import React from 'react';
import {
    Button,
   Label
} from 'reactstrap';

import * as API_PLANS from "../../medicationPlan/medicationPlan-api";
import * as API_PRESCRIPTIONS from "../../medicationPlan/medicationPrescription-api";


class PatientMedicationPlans extends React.Component {

    constructor(props) {
        super(props);
        this.takeMedicine = this.takeMedicine.bind(this);
        this.dontTakeMedicine= this.dontTakeMedicine.bind(this);

        this.state = {
            hour: this.props.hour,

            prescriptions:[],
            plan:this.props.medicationPlan,

            isLoaded: false,

            id: 0,
            selected: false,
            collapseForm: false,
            collapseFormUpdate:false,
            tableData: [],
            errorStatus: 0,
            error: null,
            caregivers:[]
        }
        this.reload = this.reload.bind(this);

    }
    componentDidMount() {
        this.fetchMedicalPrescriptions();
    }

    reload() {
        if(this.state.isLoadedPlans) {
            this.setState({
                show: false
            });
            this.fetchMedicalPrescriptions();
        }
    }

    fetchMedicalPrescriptions(){

        return API_PLANS.getMedicationPrescriptions(this.state.plan.id,(result, status, err) => {
            if (result !== null && status === 200) {
                for(let i=0;i<result.length;i++) {
                    let tempIntake = result[i].intake.split("-");
                    console.log("Din ficecare prescriptie "+tempIntake[0]+" "+parseInt(this.state.hour))
                    if(parseInt(tempIntake[0])<=parseInt(this.state.hour) && parseInt(tempIntake[1])>parseInt(this.state.hour)) {
                        {
                            let prescriptionEnhanced={
                                id:result[i].id,
                                intake:result[i].intake,
                                medication:result[i].medication,
                                medicationPlan:result[i].medicationPlan,
                                administrated:result[i].administrated,
                                showPrescription:true
                            }
                            this.state.prescriptions.push(prescriptionEnhanced)
                            console.log("Din ficecare prescriptie "+prescriptionEnhanced.administrated+" intake"+prescriptionEnhanced.intake)
                        }
                    }
                }
                this.setState({
                    //prescriptions: result,
                    show: true
                })
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    updatePatient(medicationPrescription) {
        return API_PRESCRIPTIONS.updateMedicationPrescription(medicationPrescription,medicationPrescription.id,(result, status, error) => {
            if (result !== null && (status === 202)) {
                console.log("Successfully updated medicationPlan with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    takeMedicine(prescription){
        prescription.showPrescription=false
        let medicationPrescriptionUpdate={
            id: prescription.id,
            administrated: true
        }
        this.updatePatient(medicationPrescriptionUpdate)
    }

    dontTakeMedicine(prescription){
        prescription.showPrescription=false
        let medicationPrescriptionUpdate={
            id: prescription.id,
            administrated: false
        }
        this.updatePatient(medicationPrescriptionUpdate)
    }

    render() {

        //  console.log(this.props.plans.indexOf(0));
        console.log("din Planu medica  prescriptiile sunt"+ this.state.plan.id+ "  ");
        console.log(this.state.prescriptions);

        ;

        return (
            <div style={{padding:'10 px', backgroundColor:'#ede7d8'}}>
                { this.state.show && this.state.prescriptions.map(
                    prescription=> (
                        <div style={{backgroundColor:'#bfe1f1', paddingLeft:'20px'} }key={prescription.id}>
                            { (!(prescription.administrated==='false')) && (!(prescription.administrated==='true'))  && (prescription.showPrescription) &&
                            <div>
                            <Label sm={{offset: 1}}><b>{prescription.medication.name}</b>
                                {"  "+prescription.medication.dosage}<i>  Intake:</i>{"   " + prescription.intake +" "}
                                <i>Side effects:</i> {"   "+prescription.medication.sideEffects}
                            </Label>
                            <Button style={{marginLeft:'10px',marginRight:'10px',padding:'2px'}} onClick={() => this.takeMedicine(prescription)}>Take medication</Button>
                            <Button style={{marginLeft:'10px',marginRight:'10px',padding:'2px'}} onClick={() => this.dontTakeMedicine(prescription)}>Don't take medication</Button>
                            </div>}
                            </div>
                    ))}
            </div>
        )

    }
}
export default PatientMedicationPlans;
