import {HOST} from '../commons/hosts';
import RestApiClient from "../commons/api/rest-client";

const endpoint = {
    user: '/pillbox/user'
};


function getUserByDetails(username, password ,callback){
    let request = new Request(HOST.backend_api + endpoint.user +"/"+ username+"/"+password, {
        method: 'GET'
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getUserByDetails
};
