import {HOST} from '../commons/hosts';
import RestApiClient from "../commons/api/rest-client";

const endpoint = {
    medicationPrescription:'/pillbox/medicationPrescription'
};

function updateMedicationPrescription(medicationPrescription,params, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPrescription +'/'+ params, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationPrescription)
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}


export {
    updateMedicationPrescription
};
