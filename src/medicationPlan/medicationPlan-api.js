import {HOST} from '../commons/hosts';
import RestApiClient from "../commons/api/rest-client";


const endpoint = {
    medicationPlan: '/pillbox/medicationPlan',
    medicationPrescription:'/medicationPrescription'
};



function getMedicationPrescriptions(id, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + "/"+id+ endpoint.medicationPrescription, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}



export {
    getMedicationPrescriptions
};
