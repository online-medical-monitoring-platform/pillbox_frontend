import React from 'react'
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import Persons from './person-data/person/persons'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import LoginContainer from "./login/login-container";
import PatientPage from "./patient/patient-page";

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <Switch>
                        <Route
                            exact
                            path='/'
                            render={() => <Redirect to='/login'/>}
                        />

                        <Route
                            exact
                            path='/login'
                            render={() => <LoginContainer/>}
                        />

                        <Route
                            exact
                            path='/patient'
                            render={() =>localStorage.getItem('role') === 'patient' ? (<PatientPage/>)
                                :(
                                    <Redirect to='/login'/>
                                )}
                        />

                        <Route
                            exact
                            path='/persons'
                            render={() => <Persons/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
